package ant.tasks.ifaces;

import javax.xml.namespace.QName;
import javax.xml.stream.events.XMLEvent;

public interface BuildCheck {
	QName PROJECT_TAG = new QName("project");
	QName TARGET_TAG = new QName("target");
	QName NAME_ATTR = new QName("name");
	QName DEFAULT_ATTR = new QName("default");
	QName DEPENDS_ATTR = new QName("depends");

	String check(XMLEvent xmlEvent);

}
