package ant.tasks.checks;

import java.text.MessageFormat;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import ant.tasks.ifaces.BuildCheck;

public class DefaultBuildCheck implements BuildCheck {
	public final static String FAIL_MESSAGE1 = "line {0}: there's no default attribute in tag project";
	public final static String FAIL_MESSAGE2 = "buildfile: there's no default target in build file";
	private String projectDefaultAttr;
	private boolean isContainDefaultTarget;

	@Override
	public String check(XMLEvent xmlEvent) {
		int eventType = xmlEvent.getEventType();
		switch (eventType) {
		case XMLStreamConstants.START_ELEMENT:
			return startElement(xmlEvent.asStartElement());
		case XMLStreamConstants.END_DOCUMENT:
			return endDocument();
		case XMLStreamConstants.START_DOCUMENT:
			projectDefaultAttr = "main";
			isContainDefaultTarget = false;
		}
		return null;
	}
	
	private String startElement(StartElement element) {
		QName nameElement = element.getName();
		if (PROJECT_TAG.equals(nameElement)) {
			Attribute defaultAttr = element.getAttributeByName(DEFAULT_ATTR);
			if (defaultAttr==null) {
				return MessageFormat.format(FAIL_MESSAGE1,element.getLocation().getLineNumber());
			}
			projectDefaultAttr = defaultAttr.getValue();
		}
		if (TARGET_TAG.equals(nameElement)) {
			Attribute nameAttr = element.getAttributeByName(NAME_ATTR);
			if (nameAttr!=null && projectDefaultAttr.equals(nameAttr.getValue())) {
				isContainDefaultTarget = true;
			}
		}
		return null;
	}
			
	private String endDocument() {
		if (isContainDefaultTarget) {
			return null;
		} else {
			return FAIL_MESSAGE2;
		}
	}
}
