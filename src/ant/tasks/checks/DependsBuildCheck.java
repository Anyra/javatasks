package ant.tasks.checks;

import java.text.MessageFormat;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import ant.tasks.ifaces.BuildCheck;

public class DependsBuildCheck implements BuildCheck {
	public final static String FAIL_MESSAGE = "line {0}: no default target contains depends attribute";
	private String projectDefaultAttr;

	@Override
	public String check(XMLEvent xmlEvent) {
		int eventType = xmlEvent.getEventType();
		switch (eventType) {
		case XMLStreamConstants.START_ELEMENT:
			return startElement(xmlEvent.asStartElement());
		case XMLStreamConstants.START_DOCUMENT:
			projectDefaultAttr = "main";
		}
		return null;
	}
	
	private String startElement(StartElement element) {
		QName nameElement = element.getName();
		if (PROJECT_TAG.equals(nameElement)) {
			Attribute defaultAttr = element.getAttributeByName(DEFAULT_ATTR);
			if (defaultAttr!=null) {
				projectDefaultAttr = defaultAttr.getValue();
			}
		}
		if (TARGET_TAG.equals(nameElement)) {
			Attribute nameAttr = element.getAttributeByName(NAME_ATTR);
			Attribute dependsAttr = element.getAttributeByName(DEPENDS_ATTR);
			if (dependsAttr!=null && (nameAttr!=null && !projectDefaultAttr.equals(nameAttr.getValue()))) {
				return MessageFormat.format(FAIL_MESSAGE,element.getLocation().getLineNumber());
			}
		}
		return null;
	}
}
