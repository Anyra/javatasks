package ant.tasks.checks;

import java.text.MessageFormat;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import ant.tasks.ifaces.BuildCheck;

public class NamesBuildCheck implements BuildCheck {
	public final static String FAIL_MESSAGE = "line {0}: name contains only symbols {1}";
	public final static char WRONG_SYMBOL = '-';

	@Override
	public String check(XMLEvent xmlEvent) {
		int eventType = xmlEvent.getEventType();
		if (eventType == XMLStreamConstants.START_ELEMENT) {
			return startElement(xmlEvent.asStartElement());
		}
		return null;
	}

	private String startElement(StartElement element) {
		Attribute nameAttr = element.getAttributeByName(NAME_ATTR);
		if (nameAttr!=null) {
			if (!checkName(nameAttr.getValue())) {
				return MessageFormat.format(FAIL_MESSAGE, element.getLocation().getLineNumber(), WRONG_SYMBOL);
			}
		}
		return null;
	}

	private boolean checkName(String name) {
		if (name.length() == 0) {
			return true;
		}
		for (int i = 0; i < name.length(); i++) {
			if (name.charAt(i) != WRONG_SYMBOL) {
				return true;
			}
		}
		return false;
	}
}
