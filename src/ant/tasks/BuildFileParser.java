package ant.tasks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import ant.tasks.ifaces.BuildCheck;

public class BuildFileParser {

private List<String> logMessages;

private XMLInputFactory factory = XMLInputFactory.newInstance();

public List<String> parse(File file, BuildCheck... buildchecks)
		throws FileNotFoundException, XMLStreamException {
	FileInputStream in = null;
	try {
		in = new FileInputStream(file);
		XMLEventReader eventReader = factory.createXMLEventReader(in);
		while (eventReader.hasNext()) {
			XMLEvent xmlEvent = eventReader.nextEvent();
			if (xmlEvent.getEventType()
				== XMLStreamConstants.START_DOCUMENT) {
				init();
			}
			for (BuildCheck buildcheck: buildchecks) {
				String buildcheckResult = buildcheck.check(xmlEvent);
				if (buildcheckResult != null) {
				logMessages.add(buildcheckResult);
				}
			}
		}
	return logMessages;
	} finally {
		try {
			if (in != null) {
				in.close();
			}	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

private void init() {
	logMessages = new ArrayList<String>();
}

}
