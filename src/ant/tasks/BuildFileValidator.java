package ant.tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import ant.tasks.checks.DefaultBuildCheck;
import ant.tasks.checks.DependsBuildCheck;
import ant.tasks.checks.NamesBuildCheck;
import ant.tasks.ifaces.BuildCheck;

/**
 * Custom task for Ant that validate a build xml.
 * @author Marina
 *
 */
public class BuildFileValidator extends Task {
/**
 * Conditions for checking.
 */
private List<BuildCheck> buildchecks = new ArrayList<BuildCheck>();
/**
 * Files for checking.
 */
private List<BuildFile> buildfiles = new ArrayList<BuildFile>();

/**
 * Info about some build xml file.
 * @author Marina
 *
 */
public class BuildFile {
/**
 * Location of build xml file.
 */
private String location;
/**
 * Set Location.
 * @param aLocation File location.
 */
public final void setLocation(final String aLocation) {
	this.location = aLocation;
}
/**
 * Return location.
 * @return Return location.
 */
public final String getLocation() {
	return location;
}
}
/**
 * Set Chechdepends attribute.
 * @param isCheckdepends Chechdepends attribute.
 */
public final void setCheckdepends(final boolean isCheckdepends) {
	if (isCheckdepends) {
		buildchecks.add(new DependsBuildCheck());
	}
}
/**
 * Set Checkdefault attribute.
 * @param isCheckdefault Checkdefault attribute.
 */
public final void setCheckdefault(final boolean isCheckdefault) {
	if (isCheckdefault) {
		buildchecks.add(new DefaultBuildCheck());
	}
}
/**
 * Set Checknames attribute.
 * @param isChecknames Checknames attribute.
 */
public final void setChecknames(final boolean isChecknames) {
	if (isChecknames) {
		buildchecks.add(new NamesBuildCheck());
	}
}
/**
 * Set attributes buildfile.
 * @return buildfile.
 */
public final BuildFile createBuildFile() {
	BuildFile buildfile = new BuildFile();
	buildfiles.add(buildfile);
	return buildfile;
}

@Override
public final void execute() throws BuildException {
	BuildFileParser parser = new BuildFileParser();
	for (BuildFile buildfile:buildfiles) {
		File file = new File(buildfile.getLocation());
		log(MessageFormat.format("Start checking buildfile {0}",
				buildfile.getLocation()));
		try {
			BuildCheck[] a = new BuildCheck[buildchecks.size()];
			List<String> fails = parser.parse(file,
					buildchecks.toArray(a));
			if (fails.size() != 0) {
				for (String fail: fails) {
					log(fail);
				}
			} else {
				log("Buildfile is correct");
			}
		} catch (FileNotFoundException e) {
			log(MessageFormat.format("File not found: {0} ",
					buildfile.getLocation()));
		} catch (XMLStreamException e) {
			log(MessageFormat.format("Wrong XML-document {0}",
					buildfile.getLocation()));
		}
	}
}

}
