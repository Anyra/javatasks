package by.gsu.epamlab;

// subject consisting of the uniform material
public class Subject {
	private String name;
	private Material material;
	private double volume;
	
	public Subject() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Subject(String name, Material material, double volume) {
		super();
		this.name = name;
		this.material = material;
		this.volume = volume;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}
	
	// calculating the subject mass
	public double getMass() {
		return volume*material.getDensity();
	}
	
	public String toString() {
		return name+";"+material+";"+volume+";"+getMass();
	}
	
}
