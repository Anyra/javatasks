import by.gsu.epamlab.Material;
import by.gsu.epamlab.Subject;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
						
		// Create the object representing the steel wire having the volume 0.03 cubic meter
		Subject subject = new Subject("wire",Material.STEEL,0.03);
		// Print the object content to the console, using toString() method
		System.out.println(subject);
		// Change the wire material on the copper (density = 8500.0) and print its mass
		subject.setMaterial(Material.COPPER);
		System.out.println("The "+subject.getName()+" mass is "+subject.getMass()+" kg.");
	}

}
